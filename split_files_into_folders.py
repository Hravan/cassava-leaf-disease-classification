import argparse

import pandas as pd

import cldc.utils as utils


def main(train_csv, train_folder, destination_folder):
    train_df = pd.read_csv(train_csv)
    filename_category_mapping = utils.get_filename_category_mapping(train_df)
    utils.split_files_into_category_folders(train_folder, destination_folder, filename_category_mapping)
    return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('train_csv')
    parser.add_argument('train_folder')
    parser.add_argument('destination_folder')
    args = parser.parse_args()

    main(args.train_csv, args.train_folder, args.destination_folder)