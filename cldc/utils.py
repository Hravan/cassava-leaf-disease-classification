import os
import shutil


def split_files_into_category_folders(source_foler, destination_folder, category_mapping):
    if not os.path.isdir(destination_folder):
        os.mkdir(destination_folder)

    categories = category_mapping.values()
    for category in categories:
        if not os.path.isdir(os.path.join(destination_folder, str(category))):
            os.mkdir(os.path.join(destination_folder, str(category)))

    for root, _, files in os.walk(source_foler):
        for file in files:
            file_category = category_mapping[file]
            shutil.copy(os.path.join(root, file), os.path.join(os.path.join(destination_folder, str(file_category)), file))
    return None


def get_filename_category_mapping(train_df):
    return {filename: category for filename, category in zip(train_df['image_id'], train_df['label'])}
